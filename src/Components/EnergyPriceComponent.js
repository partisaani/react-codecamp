import React, {Component} from 'react';

class EnergyPriceComponent extends Component {
	constructor(props) {
		super(props);
		this.updatePrice = this.updatePrice.bind(this);
		this.state = {
			price: 1
		};
	}

	updatePrice(event){
		this.setState({
			price: event.target.value
		});
	}

	render() {
		return(
			<div>
				<h5>Energy price</h5>
				<input type="number" onBlur={() => this.props.saveEnergyPrice(this.state.price)} onChange={this.updatePrice} value={this.state.price}/>
			</div>
		);
	}



}

export default EnergyPriceComponent;