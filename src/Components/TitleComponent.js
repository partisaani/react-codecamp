import React, {Component} from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faPenSquare from "@fortawesome/fontawesome-free-solid/faPenSquare";
import faCheckSquare from "@fortawesome/fontawesome-free-solid/faCheckSquare";
import './TitleComponent.css';

class TitleComponent extends Component {
    constructor(props) {
        super(props);
        this.onChangePropertyName = this.onChangePropertyName.bind(this);
        this.editPropertyName = this.editPropertyName.bind(this);

        this.state = {
            editingMode: false,
            propertyName: "Casa de Codecamp"
        };
    }

    editPropertyName() {
        this.setState({
            editingMode: !this.state.editingMode
        });
    }

    onChangePropertyName(event) {
        this.setState({
            propertyName: event.target.value
        });
    }

    render() {
        if (!this.state.editingMode) {
            return (
                <div className="title-component-wrapper d-inline-flex justify-content-center align-items-center">
                    <h1 className="d-inline-flex justify-content-center align-items-center">
                        <span>{this.state.propertyName}</span>
                        <FontAwesomeIcon className="mx-3 edit-button hand-cursor" icon={faPenSquare} onClick={this.editPropertyName}/>
                    </h1>
                </div>
            );
        } else {
            return (
                <div className="title-component-wrapper d-inline-flex justify-content-center align-items-center">
                    <h1>
                        <input type="text" onChange={this.onChangePropertyName} value={this.state.propertyName}/>
                        <FontAwesomeIcon className="mx-3 confirm-color hand-cursor" icon={faCheckSquare} onClick={this.editPropertyName}/>
                    </h1>
                </div>
            );
        }

    }
}

export default TitleComponent;