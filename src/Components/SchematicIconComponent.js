import React, {Component} from 'react';

import aquarium from '../svg/001-animal.svg'
import dishwasher from '../svg/002-dishwasher.svg'
import light from '../svg/003-light-bulb.svg'
import washingmachine from '../svg/004-washing-machine.svg'
import microwave from '../svg/005-microwave-oven.svg'
import oven from '../svg/006-small-oven.svg'
import computer from '../svg/007-computer.svg'
import television from '../svg/008-television.svg'

class SchematicIconComponent extends Component {
    iconTypes;
    constructor(props) {
        super(props);

        this.iconTypes = {
            aquarium,
            dishwasher,
            light,
            washingmachine,
            microwave,
            oven,
            computer,
            television
        };

        this.state = {
            controllable: this.props.controllable
        };
    }

    render() {
        return (
            <span className="mx-1 mt-2">
                <object type="image/svg+xml" data={this.iconTypes[this.state.controllable.type]} height="20px" width="20px">Your browser does not support SVGs</object>
            </span>
            );
    }

}

export default SchematicIconComponent;