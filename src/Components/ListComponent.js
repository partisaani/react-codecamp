import React, {Component} from 'react';
import './ListComponent.css';
import Toggle from 'react-toggle'

class ListComponent extends Component {
    render() {
        if (this.props.rooms) {
            return (
                <div className="list-component-wrapper">
                    <div> {this.props.rooms.map(room =>
                        <div key={room.roomId}>
                            <div className="list-item noselect hand-cursor" onClick={() => this.props.changeIsActiveStatus(room.roomId, room.type)}>
                                <span>{room.type}</span>
                            </div>
                            <div className={room.isActive ? 'submenu-open' : 'submenu-closed'}> {room.controllables.map(controllable =>
                                <div className="row" key={controllable.controllableId}>
                                    <div className="col text-right align-middle">
                                        <span>{controllable.name}</span>
                                    </div>
                                    <div className="col text-left align-middle pl-0">
                                        <Toggle
                                            className="align-middle"
                                            disabled={controllable.isDisabled}
                                            checked={controllable.isPowered}
                                            onChange={() => this.props.changeControllableStatus(room.roomId, controllable.controllableId, false)}
                                        />
                                        <span className="text-right align-middle ml-2">
                                            <span className="align-middle">Standby</span>
                                            <input className="align-middle ml-2" disabled={controllable.isDisabled} onChange={() => this.props.changeControllableStatus(room.roomId, controllable.controllableId, true)} type="checkbox"  name="standby"/>
                                        </span>
                                    </div>
                                </div>)}
                            </div>
                        </div>)}
                    </div>
                </div>
            )
        } else {
            return (
                <div className="list-component-wrapper border border-dark">No rooms found :(</div>
            );
        }

    }
}
export default ListComponent;