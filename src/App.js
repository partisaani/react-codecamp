//Libraries

import React, {Component} from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faCog from '@fortawesome/fontawesome-free-solid/faCog';
import Toggle from 'react-toggle'
import moment from 'moment';
import 'moment/locale/fi';
import NumericInput from 'react-numeric-input';

// Components
import ListComponent from './Components/ListComponent.js';
import SchematicComponent from './Components/SchematicComponent.js';
import TitleComponent from './Components/TitleComponent.js';
import api from './api';
// import GraphComponent from './Components/graphComponent.js';

// Styling
import "react-toggle/style.css"
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);

        // Binds
        this.changeControllableStatus = this.changeControllableStatus.bind(this);
        this.changeIsActiveStatus = this.changeIsActiveStatus.bind(this);
        this.toggleMasterPower = this.toggleMasterPower.bind(this);
        this.updateCurrentElectricityCost = this.updateCurrentElectricityCost.bind(this);
        this.deepCopy = this.deepCopy.bind(this);

        moment.locale('fi');

        this.state = {
            reservationStart: null,
            reservationEnd: null,
            rooms: null,
            graphs: null,
            editPropertyName: false,
            roomNames: "",
            masterPower: true,
            isReserved: false,
            totalUsage: 0,
            unitPrice: 50,
            totalPrice: 0
        }
    }

    componentDidMount() {
        let usage = 0;

        setTimeout(() => {
            api.getRooms().then(data => {
                const reserved = moment().isBetween(moment(data.reservation.startDate), moment(data.reservation.endDate));
                data.rooms.map(room => {
                    room.isActive = false;
                    room.controllables.map(controllable => {
                        controllable.isDisabled = !reserved;
                        if (controllable.isPowered) {
                            usage = usage + controllable.idlePower;
                        }
                        return 1;
                    });
                    return 1;
                });
                this.setState({
                    isReserved: reserved,
                    masterPower: reserved,
                    reservationStart: data.reservation.startDate,
                    reservationEnd: data.reservation.endDate,
                    rooms: data.rooms,
                    totalUsage: usage
                })
            })
        }, 1250);

        api.getGraphInfo().then(graphs => {
            this.setState({
                graphs: graphs
            })
        });
    }

    changeIsActiveStatus(roomId, type) {
        let updatedRooms = this.deepCopy(this.state.rooms);
        updatedRooms = updatedRooms.map(room => {
            room.isActive = (room.roomId === roomId);
            return room;
        });
        this.setState({
            rooms: updatedRooms,
            roomNames: type
        });
    }

    changeControllableStatus(roomId, controllableId, fromStandBy) {
        let updatedRooms = this.deepCopy(this.state.rooms);
        let updatedUsage = this.state.totalUsage.valueOf();
        for (let room of updatedRooms) {
            if (room.roomId === roomId) {
                for (let controllable of room.controllables) {
                    if (controllable.controllableId === controllableId) {
                        if (fromStandBy) {
                            controllable.onIdle = !controllable.onIdle;
                            if (controllable.onIdle) {
                                if (controllable.isPowered) {
                                    updatedUsage = updatedUsage - controllable.idlePower + controllable.idlePower / 5;
                                }
                            } else {
                                if (controllable.isPowered) {
                                    updatedUsage = updatedUsage - controllable.idlePower / 5 + controllable.idlePower;
                                }
                            }
                        } else {
                            controllable.isPowered = !controllable.isPowered;
                            if (controllable.onIdle) {
                                if (controllable.isPowered) {
                                    updatedUsage = updatedUsage + controllable.idlePower / 5;
                                } else {
                                    updatedUsage = updatedUsage - controllable.idlePower / 5;
                                }
                            } else {
                                if (controllable.isPowered) {
                                    updatedUsage = updatedUsage + controllable.idlePower;
                                } else {
                                    updatedUsage = updatedUsage - controllable.idlePower;
                                }
                            }

                        }
                        this.setState({
                            rooms: updatedRooms,
                            totalUsage: updatedUsage
                        });
                    }
                }
            }
        }
    }

    updateCurrentElectricityCost(value) {
        this.setState({
            unitPrice: value
        });
    }

    toggleMasterPower() {
        let newRooms = this.deepCopy(this.state.rooms);
        newRooms.map(room => {
            room.controllables.map(controllable => {
                if (this.state.masterPower) {
                    controllable.isPowered = false;
                    controllable.isDisabled = true;
                } else {
                    controllable.isDisabled = false;
                }
                return 1;
            });
            return 1;
        });

        this.setState({
            masterPower: !this.state.masterPower,
            rooms: newRooms,
            totalUsage: 0
        });
    }

    render() {
        const rooms = this.state.rooms;
        if (rooms == null) {
            return (
                    <div className="container-fluid">
                        <div className="d-flex flex-row justify-content-center main-loading align-items-center">
                            <FontAwesomeIcon
                                className="mx-2"
                                icon={faCog}
                                spin/>
                            <span>Loading rooms...</span>
                        </div>
                    </div>
            );
        } else {
            return (
                    <div className="container-fluid">
                        <div className="text-center main-content-wrapper">
                            <TitleComponent/>
                            <h5>
                                <span>Current electricity cost per hour: </span>
                                <span className="font-weight-bold">{this.state.totalUsage * this.state.unitPrice}</span>
                            </h5>
                            <div className="row">
                                <div className="col-md-3 px-2">
                                    <ListComponent
                                        changeIsActiveStatus={this.changeIsActiveStatus}
                                        changeControllableStatus={this.changeControllableStatus}
                                        rooms={this.state.rooms}/>
                                    <div className="row mt-5 mx-2">
                                        <div className="col">
                                            <h5>Master power</h5>
                                            <Toggle
                                                disabled={!this.state.isReserved}
                                                checked={this.state.masterPower}
                                                onChange={this.toggleMasterPower}
                                            />
                                        </div>
                                        <div className="col">
                                            <h5>Electricity Price</h5>
                                            <NumericInput
                                                mobile
                                                className="form-control"
                                                min={0}
                                                value={this.state.unitPrice}
                                                max={999}
                                                maxLength={3}
                                                onChange={this.updateCurrentElectricityCost}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col px-0">
                                    <SchematicComponent changeIsActiveStatus={this.changeIsActiveStatus}
                                                        rooms={this.state.rooms}/>
                                </div>
                            </div>
                            {/*<div className="row">*/}
                            {/*<div className="col-md-4 px-0">*/}
                            {/*<GraphComponent graphs={this.state.graphs} roomNames={this.state.roomNames}/>*/}
                            {/*</div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
            );
        }
    }

    deepCopy(object) {
        return JSON.parse(JSON.stringify(object));
    }

}

export default App;
